use std::net::{TcpStream};
use std::io::{Read, Write};
use std::io;
use base64::{encode, decode};
use std::str;


fn main() {
    match TcpStream::connect("localhost:3333") {
        Ok(mut stream) => {
            println!("Successfully connected to server in port 3333");
            loop{
            println!("Que voulez-vous envoyer ?");
            let mut msg = String::new();
            match io::stdin().read_line(&mut msg) {
                Ok(_) => {
                    let encoded = encode(msg);
                    stream.write(encoded.as_bytes()).unwrap();
                },
                Err(error) => {
                    println!("error: {}", error);
                }
               
            }
            let mut data = [0 as u8; 50]; 
            match stream.read(&mut data) {
                Ok(_) => {
                        let text = decode(str::from_utf8(&data).unwrap_or("aGVsbG8gd29ybGQ="));
                        println!(" {:?}", text);
                    
                },
                Err(e) => {
                    println!("Failed to receive data: {}", e);
                }
            } 
             
        }
        },
        Err(e) => {
            println!("Failed to connect: {}", e);
        }
    }
    println!("Terminated.");

   
}
